package GA;

import java.text.SimpleDateFormat;
import java.util.Date;


public class Program implements GA_Param {

    public static void printNowTime() {
        SimpleDateFormat df = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss SSS\n");
        System.out.print(df.format(new Date()));
    }

    public static void run(boolean isOptimized) throws Exception {
        GA_Solution gs = new GA_Solution();
        for (int i = 0; i < runTimes; ++i) {
            gs.run(isOptimized);
        }

        if (isOptimized) {
            int optimizedLevel = 4;
            gs.optimized(optimizedLevel);
            gs.printBestInfo();
        }
    }

    public static void main(String[] args) throws Exception {
        printNowTime();

        run(true);

        printNowTime();
    }
}
